package org.example;

import org.example.model.Account;
import org.example.model.Bank;
import org.example.model.Card;
import org.example.model.Customer;
import org.example.model.enums.BankName;
import org.example.model.enums.Currency;
import org.example.service.CashService;
import org.example.service.TransferService;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        Customer firstCus = new Customer(1, "Orxan", "Abdullayev",
                new Card("5001", BigDecimal.valueOf(10000), Currency.AZN),
                new Account("AZN5001",BigDecimal.valueOf(0),Currency.AZN),
                31,false,
                new Bank(1, BankName.VTB));

        Customer secCus = new Customer(2, "Faxri", "Nuriyev",
                new Card("5533", BigDecimal.valueOf(1200), Currency.AZN),
                new Account("AZN5533",BigDecimal.valueOf(12000),Currency.AZN),
                31,false,
                new Bank(1, BankName.VTB));

        firstCus.blockCustomer(firstCus);

        System.out.println(firstCus);

//        CashService cshServ = new CashService();
//        cshServ.cashInAccount(BigDecimal.valueOf(650),firstCus);
//        System.out.println(firstCus.getAccount().getBalance());

        TransferService tshServ = new TransferService();
        tshServ.transfer(firstCus, secCus, BigDecimal.valueOf(200));
        System.out.println(firstCus.getCard().getBalance());
    }
}