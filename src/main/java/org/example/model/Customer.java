package org.example.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Customer {
    private long id;
    private String name;
    private String surName;
    private Card card;
    private Account account;
    private int age;
    private boolean isBlocked;
    private Bank bank;

    public void blockCustomer (Customer customer) {
        customer.setBlocked(true);
    }
    public void unBlockCustomer (Customer customer) {
        customer.setBlocked(false);
    }

}
