package org.example.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BankName {
    VTB ("Bank VTB Azerbaijan"),
    BankOfBaku ("Bankf of Baku"),
    ABB ("Azerbaijan International Bank");


    private final String getBankName;

}
