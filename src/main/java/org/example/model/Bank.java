package org.example.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.example.model.enums.BankName;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Bank {
    private long customerId;
    private BankName bankName;

}
