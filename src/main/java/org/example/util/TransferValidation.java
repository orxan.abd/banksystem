package org.example.util;

import org.example.model.Card;
import org.example.model.Customer;
import org.example.model.enums.BankName;

import java.math.BigDecimal;

public class TransferValidation {
    public void controlTransfer(BigDecimal amount, Customer fromCustomer, boolean isCard) {
        if (fromCustomer.isBlocked()) {
            throw new RuntimeException("Customer is blocked");
        } else if (isCard) {
            checkAmounts(amount);
            checkBalance(fromCustomer, amount);
        } else {
            checkAmounts(amount);
            checkBalance(fromCustomer, amount);
        }
        if (fromCustomer.getBank().getBankName().equals(BankName.VTB)){
            System.out.println("Employer of VTB acting transfer");
        }
        if (amount.compareTo(BigDecimal.valueOf(100)) >= 0) {
            System.out.println("Amount of transfer is over 100");
        }
    }

    public void checkAmounts(BigDecimal amount) {
        if (amount.equals(BigDecimal.valueOf(0))) {
            throw new RuntimeException("Zero amount is not allowed");
        }
    }

    public int checkBalance (Customer fromCustomer, BigDecimal amount) {
        int result = fromCustomer.getAccount().getBalance().compareTo(amount);
        int cardResult = fromCustomer.getCard().getBalance().compareTo(amount);
        int returnResult = -1;
        if (result < 0) {
            returnResult = 0;
        }
        if (cardResult < 0) {
            throw new RuntimeException("Insufficient balance");
            }
        zeroCheckBalance(fromCustomer.getCard().getBalance());
        if (returnResult != 0) {
            zeroCheckBalance(fromCustomer.getAccount().getBalance());
        }
        returnResult =  1;
        return returnResult;
    }

    public void zeroCheckBalance (BigDecimal balance) {
        if (balance.equals(BigDecimal.valueOf(0))) {
            throw new RuntimeException("Zero balance");
        }
    }

    public void checkCurrency (Customer firstCustomer, Customer secondCustomer) {
        if ( !firstCustomer.getAccount().getCurrency().equals(secondCustomer.getAccount().getCurrency())) {
            throw new RuntimeException( ("Account currency is not equals"));
        }
        else if (!firstCustomer.getCard().getCurrency().equals(secondCustomer.getCard().getCurrency())) {
            throw new RuntimeException("Card currency is not equals");
        }
    }

}


