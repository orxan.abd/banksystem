package org.example.service;

import org.example.model.Customer;
import org.example.model.enums.Currency;
import org.example.util.TransferValidation;

import java.math.BigDecimal;

public class TransferService {

    public void transfer (Customer from, Customer to, BigDecimal amount) {
        TransferValidation transferValidation = new TransferValidation();
        transferValidation.controlTransfer(amount,from,true);
        transferValidation.checkCurrency(from,to);
        int i = transferValidation.checkBalance(from, amount);
        if (i == 0) {
            from.getAccount().setBalance(from.getAccount().getBalance().subtract(amount));
            to.getAccount().setBalance(to.getAccount().getBalance().add(amount));
        }
        else {
            from.getCard().setBalance(from.getCard().getBalance().subtract(amount));
            to.getCard().setBalance(to.getCard().getBalance().add(amount));
        }
//        from.getAccount().setBalance(from.getAccount().getBalance().subtract(amount));
//        to.getAccount().setBalance(to.getAccount().getBalance().add(amount));

    }
}
