package org.example.service;

import org.example.model.Customer;
import org.example.util.TransferValidation;

import java.math.BigDecimal;

public class CashService {
    public Customer cashInCard(BigDecimal amount, Customer customer) {
        customer.getCard().setBalance(addBalance(amount, customer.getCard().getBalance()));
        return customer;
    }

    public Customer cashInAccount(BigDecimal amount, Customer customer) {
        customer.getAccount().setBalance(addBalance(amount, customer.getAccount().getBalance()));
        return customer;
    }

    private BigDecimal addBalance(BigDecimal amount, BigDecimal actualBalance) {
        return actualBalance.add(amount);
    }

    private BigDecimal divideBalance(BigDecimal amount, BigDecimal actualBalance) {
        return actualBalance.divide(amount);
    }

    public Customer CashOutCard(BigDecimal amount, Customer customer) {
        BigDecimal actualBalance = customer.getCard().getBalance();
        TransferValidation transferValidation = new TransferValidation();
        transferValidation.checkAmounts(amount);
        transferValidation.checkBalance(customer, amount);
        customer.getCard().setBalance(divideBalance(amount, actualBalance));
        return customer;
    }

    public Customer CashOutAccount(BigDecimal amount, Customer customer) {
        BigDecimal actualBalance = customer.getAccount().getBalance();
        TransferValidation transferValidation = new TransferValidation();
        transferValidation.checkAmounts(amount);
        transferValidation.checkBalance(customer, amount);
        customer.getAccount().setBalance(divideBalance(amount, actualBalance));
        return customer;
    }

}
